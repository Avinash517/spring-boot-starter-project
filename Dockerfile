#FROM openjdk:8-jdk-alpine
#VOLUME /tmp
#ADD WebContent/lib/*.jar app.jar
#COPY WebContent lib ./.jar app.jar
#EXPOSE 8080
#ARG WAR_FILE=target/StarterKit-0.0.1-SNAPSHOT.war
#ADD ${WAR_FILE} Starter Kit.war
#ADD WebContent lib ./*.jar app.jar
#ADD target/*.jar app.jar
#ADD /target/StarterKit-0.0.1-SNAPSHOT.jar
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-war","/Starter Kit.war"]
# From tomcat:8.0.51-jre8-alpine
# RUN rm -rf /usr/local/tomcat/webapps/*
# COPY ./target/StarterKit-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war
# CMD ["catalina.sh","run"]
FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]